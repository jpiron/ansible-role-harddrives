# Ansible common role
This role aims to handle hard drives. It will manage the mountpoint creations, the fstab population and the drives spindown.

## Requirements
None.

## Role variables
* `hardrives_drives_list`: Hard drives list to manage. Defaults to empty. Should be defined at playbook level.

## TODO
* Drive spindown timeout could be hanlded per drive rather than globally.
